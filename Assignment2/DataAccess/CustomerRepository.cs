﻿using System.Collections.Generic;
using Assignment2.Models;
using Microsoft.Data.SqlClient;

namespace Assignment2.DataAccess
{
    class CustomerRepository : ICustomerRepository
    {
        public CustomerRepository()
        {
            Builder = new SqlConnectionStringBuilder()
            {
                DataSource = @"IN-PF2RMJ28\SQLEXPRESS",
                InitialCatalog = "Chinook",
                IntegratedSecurity = true,
                TrustServerCertificate = true
            };
        }

        public SqlConnectionStringBuilder Builder { get; set; }

        public IEnumerable<Customer> GetAllCustomers()
        {
            using SqlConnection connection = new SqlConnection(Builder.ConnectionString);
            string sql = "SELECT * FROM Customer";
            connection.Open();

            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();

            var customerList = new List<Customer>();
            while (reader.Read())
            {
                customerList.Add(new Customer
                {
                    CustomerId = reader.GetInt32(0),
                    FirstName = reader.IsDBNull(1) ? null : reader.GetString(1),
                    LastName = reader.IsDBNull(2) ? null : reader.GetString(2),
                    Country = reader.IsDBNull(7) ? null : new CustomerCountry { Name = reader.GetString(7) },
                    PostalCode = reader.IsDBNull(8) ? null : reader.GetString(8),
                    PhoneNumber = reader.IsDBNull(9) ? null : reader.GetString(9),
                    Email = reader.IsDBNull(10) ? null : reader.GetString(10)
                });
            }
            connection.Close();
            return customerList;
        }

        public Customer GetCustomerById(int id)
        {
            using SqlConnection connection = new SqlConnection(Builder.ConnectionString);
            string sql = $"SELECT * FROM Customer WHERE CustomerId = {id}";
            connection.Open();

            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            var customer = new Customer
            {
                CustomerId = reader.GetInt32(0),
                FirstName = reader.IsDBNull(1) ? null : reader.GetString(1),
                LastName = reader.IsDBNull(2) ? null : reader.GetString(2),
                Country = reader.IsDBNull(7) ? null : new CustomerCountry { Name = reader.GetString(7) },
                PostalCode = reader.IsDBNull(8) ? null : reader.GetString(8),
                PhoneNumber = reader.IsDBNull(9) ? null : reader.GetString(9),
                Email = reader.IsDBNull(10) ? null : reader.GetString(10)
            };

            connection.Close();
            return customer;
        }

        public Customer GetCustomerByName(string name)
        {

            using SqlConnection connection = new SqlConnection(Builder.ConnectionString);
            string sql = $"SELECT * FROM Customer WHERE FirstName Like '{name}' OR LastName LIKE '{name}'";
            connection.Open();

            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();

            reader.Read();
            var customer = new Customer
            {
                CustomerId = reader.GetInt32(0),
                FirstName = reader.IsDBNull(1) ? null : reader.GetString(1),
                LastName = reader.IsDBNull(2) ? null : reader.GetString(2),
                Country = reader.IsDBNull(7) ? null : new CustomerCountry { Name = reader.GetString(7) },
                PostalCode = reader.IsDBNull(8) ? null : reader.GetString(8),
                PhoneNumber = reader.IsDBNull(9) ? null : reader.GetString(9),
                Email = reader.IsDBNull(10) ? null : reader.GetString(10)
            };

            connection.Close();
            return customer;
        }

        public IEnumerable<Customer> GetCustomerPage(int limit, int offset)
        {
            using SqlConnection connection = new SqlConnection(Builder.ConnectionString);
            string sql = $"SELECT * FROM Customer ORDER BY CustomerId OFFSET {offset} ROWS FETCH NEXT {limit} ROWS ONLY";
            connection.Open();

            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();

            var customerList = new List<Customer>();

            for (var i = 0; i < limit; i++)
            {
                reader.Read();
                customerList.Add(new Customer
                {
                    CustomerId = reader.GetInt32(0),
                    FirstName = reader.IsDBNull(1) ? null : reader.GetString(1),
                    LastName = reader.IsDBNull(2) ? null : reader.GetString(2),
                    Country = reader.IsDBNull(7) ? null : new CustomerCountry { Name = reader.GetString(7) },
                    PostalCode = reader.IsDBNull(8) ? null : reader.GetString(8),
                    PhoneNumber = reader.IsDBNull(9) ? null : reader.GetString(9),
                    Email = reader.IsDBNull(10) ? null : reader.GetString(10)
                });
                ;
            }
            connection.Close();
            return customerList;
        }

        public void CreateCustomer(
            string firstName,
            string lastName,
            CustomerCountry country,
            string postalCode,
            string phoneNumber,
            string email)
        {
            using SqlConnection connection = new SqlConnection(Builder.ConnectionString);
            string sql = $"INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                $"Values (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            connection.Open();

            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@FirstName", firstName);
            command.Parameters.AddWithValue("@LastName", lastName);
            command.Parameters.AddWithValue("@Country", country.Name);
            command.Parameters.AddWithValue("@PostalCode", postalCode);
            command.Parameters.AddWithValue("@Phone", phoneNumber);
            command.Parameters.AddWithValue("@Email", email);

            command.ExecuteNonQuery();
        }
        public void UpdateCustomer(
            string firstName,
            int customerId)
        {
            using SqlConnection connection = new SqlConnection(Builder.ConnectionString);
            string sql = $"UPDATE Customer SET FirstName = @newFirstName WHERE CustomerId = @CustomerId";
            connection.Open();

            using SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@newFirstName", firstName);
            command.Parameters.AddWithValue("@CustomerId", customerId);

            command.ExecuteNonQuery();
        }
        public IEnumerable<CustomerCountry> GetCustomerCountByCountry()
        {
            using SqlConnection connection = new SqlConnection(Builder.ConnectionString);
            string sql = $"SELECT Country, Count(*) AS CNT " +
                $"FROM Customer GROUP BY Country " +
                $"HAVING COUNT(*) > 0 " +
                $"ORDER BY CNT DESC";
            connection.Open();

            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();

            List<CustomerCountry> customerCountList = new List<CustomerCountry>();
            while (reader.Read())
            {
                customerCountList.Add(new CustomerCountry
                {
                    Name = reader.GetString(0),
                    Count = reader.GetInt32(1)
                });
            }

            connection.Close();
            return customerCountList;
        }

        public IEnumerable<CustomerSpender> GetInvoiceCountByCustomer()
        {
            using SqlConnection connection = new SqlConnection(Builder.ConnectionString);
            string sql = $"SELECT DISTINCT SUM(Invoice.Total) over (partition by Customer.FirstName) as totalSum, Customer.FirstName " +
                $"FROM Customer Join Invoice On Customer.CustomerId = Invoice.CustomerId " +
                $"ORDER BY totalSum desc";
            connection.Open();

            using SqlCommand command = new SqlCommand(sql, connection);
            using SqlDataReader reader = command.ExecuteReader();

            List<CustomerSpender> customerSpenderList = new List<CustomerSpender>();
            while (reader.Read())
            {
                customerSpenderList.Add(new CustomerSpender
                {
                    Count = reader.GetDecimal(0),
                    Country = new CustomerCountry { Name = reader.GetString(1) }
                });
            }

            connection.Close();
            return customerSpenderList;
        }

        //This implementation does not cater for Customers with multiple genres tied for most popular
        public CustomerGenre GetMostPopularGenreByCustomerId(int id)
        {
            using SqlConnection connection = new SqlConnection(Builder.ConnectionString);
            string sqlGetGenreId = $"SELECT Track.GenreId, Count(Track.GenreId) as genreCount" +
                $" FROM InvoiceLine Join Invoice On InvoiceLine.InvoiceId = Invoice.InvoiceId Join Track On Track.TrackId = InvoiceLine.TrackId" +
                $" Where Invoice.CustomerId = {id}" +
                $" Group by Track.GenreId, Invoice.CustomerId" +
                $" ORDER BY genreCount desc" +
                $" OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY";

            connection.Open();

            using SqlCommand idCommand = new SqlCommand(sqlGetGenreId, connection);
            using SqlDataReader idReader = idCommand.ExecuteReader();
            idReader.Read();

            var genreId = idReader.GetInt32(0);
            string sqlGetGenreName = $" Select Genre.Name" +
                $" from Genre" +
                $" Where Genre.GenreId = {genreId}";
            idReader.Close();

            using SqlCommand nameCommand = new SqlCommand(sqlGetGenreName, connection);
            using SqlDataReader nameReader = nameCommand.ExecuteReader();
            nameReader.Read();

            var genreName = nameReader.GetString(0);
            var genre = new CustomerGenre
            {
                Id = genreId,
                Name = genreName
            };

            connection.Close();
            return genre;
        }
    }
}
