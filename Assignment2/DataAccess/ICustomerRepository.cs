﻿using System.Collections.Generic;
using Assignment2.Models;
using Microsoft.Data.SqlClient;

namespace Assignment2.DataAccess
{
    interface ICustomerRepository
    {

        public SqlConnectionStringBuilder Builder { get; set; }

        /// <summary>
        /// Gets all Customers
        /// </summary>
        /// <returns>IEnumerable with the Customers</returns>
        public IEnumerable<Customer> GetAllCustomers();

        /// <summary>
        /// Gets an individial Customer by their Id
        /// </summary>
        /// <param name="id">The Id of the desired Customer</param>
        /// <returns>A Customer</returns>
        public Customer GetCustomerById(int id);

        /// <summary>
        /// Gets an individial Customer by their name
        /// </summary>
        /// <param name="name">The name of the desired Customer</param>
        /// <returns>A Customer</returns>
        public Customer GetCustomerByName(string name);

        /// <summary>
        /// Gets a page of Customers with a given size.
        /// </summary>
        /// <param name="limit">The amount of Customers per page</param>
        /// <param name="offset">How many Customers to skip before the page</param>
        /// <returns>An IEnumerable with the Customers</returns>
        public IEnumerable<Customer> GetCustomerPage(int limit, int offset);

        /// <summary>
        /// Adds a new Customer to the database.
        /// </summary>
        /// <param name="firstName">The first name of the customer</param>
        /// <param name="lastName">The last name of the customer</param>
        /// <param name="country">The country of the customer</param>
        /// <param name="postalCode">The postalCode of the customer</param>
        /// <param name="phoneNumber">The phone number of the customer</param>
        /// <param name="email">The email of the customer</param>
        public void CreateCustomer(
            string firstName,
            string lastName,
            CustomerCountry country,
            string postalCode,
            string phoneNumber,
            string email);

        /// <summary>
        /// Updates the first name of an individual Customer
        /// </summary>
        /// <param name="firstName">The new first name of the Customer</param>
        /// <param name="customerId">The Id of the Customer</param>
        public void UpdateCustomer(
                    string firstName,
                    int customerId);

        /// <summary>
        /// Gets the amount of Customers in each Country, ordered descending
        /// </summary>
        /// <returns>An IEnumerable with the CustomerCountries</returns>
        public IEnumerable<CustomerCountry> GetCustomerCountByCountry();

        /// <summary>
        /// Gets the customers who are the highest spenders, ordered descending
        /// </summary>
        /// <returns>An IEnumerable with the CustomerSpenders</returns>
        public IEnumerable<CustomerSpender> GetInvoiceCountByCustomer();

        /// <summary>
        /// Gets the most popular genre for a given Customer
        /// </summary>
        /// <param name="id">The Id of the Customer</param>
        /// <returns>The most popular CustomerGenre</returns>
        public CustomerGenre GetMostPopularGenreByCustomerId(int id);
    }
}
