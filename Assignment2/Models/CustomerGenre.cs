﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2.Models
{
    public class CustomerGenre
    {
        public int Id { init; get; }
        public string Name { get; set; }
    }
}
