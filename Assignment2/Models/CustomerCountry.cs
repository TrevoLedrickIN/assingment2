﻿namespace Assignment2.Models
{
    public class CustomerCountry
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }
}
