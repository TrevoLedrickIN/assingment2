﻿namespace Assignment2.Models
{
    public class CustomerSpender
    {
        public decimal Count { get; set; }
        public CustomerCountry Country { init; get; }
    }
}
