CREATE TABLE Superhero(
	Id int NOT NULL Identity(1,1) PRIMARY KEY,
	Name varchar(50),
	Alias varchar(100),
	Origin varchar(100)
);

Create table Assistant(
	Id int NOT NULL Identity(1,1) PRIMARY KEY,
	Name varchar(50)
);

Create table Power(
	Id int NOT NULL Identity(1,1) PRIMARY KEY,
	Name varchar(50),
	Description varchar(250)
);