Insert into Power(Name, Description)
Values ('Fly', 'The ability to fly');

Insert into Power(Name, Description)
Values ('Laser Beam', 'Strong beam created within the suit');

Insert into Power(Name, Description)
Values ('Super Strength', 'Extraordinary strength');

Insert into Power(Name, Description)
Values ('Thunder Beam', 'Beam from the power of a thunderstorm');

Insert into SuperheroPower(SuperheroId, PowerId)
Values ((Select Id From Superhero Where Name='Tony Stark'), (Select Id From Power Where Name='Fly'));

Insert into SuperheroPower(SuperheroId, PowerId)
Values ((Select Id From Superhero Where Name='Tony Stark'), (Select Id From Power Where Name='Laser Beam'));

Insert into SuperheroPower(SuperheroId, PowerId)
Values ((Select Id From Superhero Where Name='Nicholas Lennox'), (Select Id From Power Where Name='Thunder Beam'));

Insert into SuperheroPower(SuperheroId, PowerId)
Values ((Select Id From Superhero Where Name='Nicholas Lennox'), (Select Id From Power Where Name='Super Strength'));

Insert into SuperheroPower(SuperheroId, PowerId)
Values ((Select Id From Superhero Where Name='Steve Rogers'), (Select Id From Power Where Name='Super Strength'));
