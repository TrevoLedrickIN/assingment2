Insert into Assistant(Name, SuperheroId)
Values ('Anthony Frost', (Select Id From Superhero Where Name='Steve Rogers'));

Insert into Assistant(Name, SuperheroId)
Values ('Betty Summers', (Select Id From Superhero Where Name='Tony Stark'));

Insert into Assistant(Name, SuperheroId)
Values ('Alex Smith', (Select Id From Superhero Where Name='Nicholas Lennox'));