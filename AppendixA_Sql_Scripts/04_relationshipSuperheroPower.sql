Create table SuperheroPower(
	Id int NOT NULL Identity(1,1) PRIMARY KEY,
);

Alter table SuperheroPower
Add SuperheroId int Foreign key REFERENCES Superhero(Id) NOT NULL;

Alter table SuperheroPower
Add PowerId int Foreign key REFERENCES Power(Id) NOT NULL;